# Link

Open links according to content.

## Requirements

- python
- appdirs

## How to use

- Download
- Make file executable
- Run once to create Regexes, MailFallback and LinkFallback
- Edit files
- Set as browser

## How to use from browser

- Set as email client
- Install one of the known working FOSS addons

## Known working FOSS addons

- [Send Link from context menu](https://addons.mozilla.org/en-US/firefox/addon/send-link-from-context-menu)
- [Send to Mail](https://addons.mozilla.org/en-US/firefox/addon/send-to-mail)
- [MailTo](https://addons.mozilla.org/en-US/firefox/addon/mailto-we)
- [mailto:me](https://addons.mozilla.org/en-US/firefox/addon/mailto-me)
